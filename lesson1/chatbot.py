# Building a chatbot with deep NLP

import numpy as np
import tensorflow as tf
import re
import time
from functools import reduce
import contractions


lines = open('movie_lines.txt', encoding = 'utf-8', errors = 'ignore').read().split('\n')
conversations = open('movie_conversations.txt', encoding = 'utf-8', errors = 'ignore').read().split('\n')

divider = ' +++$+++ '

id_to_line = {}
for line in lines:
    _line = line.split(divider)
    if len(_line) == 5:
        id_to_line[_line[0]] = _line[4] 
        
conversations_ids = []
for conversation in conversations[:-1]:
    _conversation = conversation.split(divider)[-1][1:-1].replace("'","").replace(" ", "")
    conversations_ids.append(_conversation.split(','))
    
    
questions = []
answers = []
for conversation in conversations_ids:
    for i in range(len(conversation) - 1):
        questions.append(id_to_line[conversation[i]])
        answers.append(id_to_line[conversation[i+1]])
        
        
def clean_text(text):
    text = text.lower()
    text = contractions.fix(text)
    text = text.strip()
    text = re.sub(r"[\[\](--)\-_\`\'()\"#/@;:*!<>{}+=~|.?,]", "", text)

    return text
   
clean_questions = []
for question in questions:
    cleaned = clean_text(question)
    if cleaned: clean_questions.append(cleaned)
 
clean_answers = []
for answer in answers:
    cleaned = clean_text(answer)
    if cleaned: clean_answers.append(cleaned)
    

split_sentences = list(map(lambda sentence: sentence.split(" "), (clean_questions + clean_answers)))
words = {}
for sentence in split_sentences:
    for word in sentence:
        cleaned = clean_text(word)
        if cleaned and (cleaned.startswith("&") != True):   
            if cleaned not in words: words[cleaned] = 1
            else: words[cleaned] = words[cleaned] + 1
    